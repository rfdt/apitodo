const {Types} = require('mongoose');

var express = require('express');
var router = express.Router();
var auth = require('../middlewares/auth');
var Todo = require('../schemas/Todo');
var User = require('../schemas/User');

router.get('/getusertodo', auth, async (req, res) => {
    try {
        creator = req.user.id;

        const todos = await Todo.find({creator: creator}).select('-__v');

        if (!todos) throw Error('Something went wrong with getting todo');

        res.status(200).send(todos);

    } catch (error) {
        res.status(400).send({msg: error.message});
    }
});


router.post('/createtodo', auth, async (req, res) => {
    const {data} = req.body;
    const creator = req.user.id;


    if (!data) return res.status(400).send({msg: "Write data field"});

    try {
        const newTodo = new Todo({
            data: data,
            creator: creator //изменить
        });

        const savedTodo = await newTodo.save();

        if (!savedTodo) throw Error('Something went wrong saving TODO');

        let savedTodoDoc = {...savedTodo._doc};

        delete savedTodoDoc['__v'];

        res.status(200).send({
            ...savedTodoDoc
        });


    } catch (error) {
        res.status(400).json({msg: error.message});
    }

});

router.get('/updatetodostatus/:id', auth, async (req, res) => {
    const id = req.params['id'];
    try {

        const todoById = await Todo.findOne({_id: id});
        if (!todoById) throw Error('Something went wrong with get todoin todo status /updatetodostatus');

        const findAndUpdate = await Todo.findByIdAndUpdate(id, {$set: {completed: !todoById.completed}}, {new: true});
        if (!findAndUpdate) throw Error('Something went wrong with updatetodo');

        const changedId = findAndUpdate._id;

        res.status(200).send({msg: "Update todo success", id: changedId});

    } catch (error) {
        res.status(400).send({msg: error.message});
    }
});

router.get('/deletetodo/:id', auth, async (req, res) => {
    const id = req.params['id'];
    try {
        const deletedToDo = await Todo.deleteOne({_id: id});
        if (!deletedToDo) throw Error("Something went wrong with deleting todo");
        console.log(deletedToDo);
        res.status(200).send({data: deletedToDo});
    } catch (error) {
        res.status(400).send({msg: error.message});
    }
});

router.get('/gettodo/:id', auth, async (req, res) => {
    try {
        const id = req.params['id'];
        const creator = req.user.id;
        console.log(id, creator);

        const todo = await Todo.findById({_id: id})

        if (!todo) throw Error('Something went wrong with getting todo');

        if (creator !== todo.creator) res.status(400).send({msg: 'ID check error'});

        res.status(200).send(todo);

    } catch (error) {
        res.status(400).send({msg: error.message});
    }
});

router.post('/addtask/:id', async (req, res) => {

    try {
        const id = req.params['id'];
        const {data} = req.body;

        const newTask = {
            data: data,
        };

        const findedById = await Todo.findById({_id: id});


        const findAndUpdate = await Todo.findByIdAndUpdate(id, {$set: {tasks: [...findedById.tasks, newTask]}}, {new: true}).select('tasks');

        res.status(200).send(findAndUpdate);

    } catch (e) {
        res.status(400).send({msg: e.message});
    }

});

router.get('/updatetaskstatus/:idtodo/:idtask', auth, async (req, res) => {
    try {
        const idtodo = req.params['idtodo'];
        const idtask = req.params['idtask'];

        const findTaskById = await Todo.findOne({_id: idtodo, 'tasks._id': Types.ObjectId(idtask)});

        if (!findTaskById) throw Error('Something went wrong with get /updatetaskstatus/:idtodo/:idtask');

        const tasks = findTaskById.tasks;

        const task = tasks.find(task => task._id == idtask);


        const updateTaskById = await Todo.findOneAndUpdate(
            {
                _id: idtodo,
                'tasks._id': Types.ObjectId(idtask)
            }, {$set: {'tasks.$.completed': !task.completed}}, {new: true});

        if (!updateTaskById) throw Error('Something went wrong with get /updatetaskstatus/:idtodo/:idtask');

        res.status(200).send({msg: 'ok'});

    } catch (error) {
        res.status(400).send({msg: error.message});
        console.log(error.message);
    }
});

router.get('/deletetask/:idtodo/:idtask', auth, async (req, res) => {
    try {
        const idtodo = req.params['idtodo'];
        const idtask = req.params['idtask'];


        const deleteTaskById = await Todo.findOneAndUpdate(
            {_id: idtodo, 'tasks._id': Types.ObjectId(idtask)}, {$pull: {"tasks": {"_id": idtask}}}, {new: true});

        if (!deleteTaskById) throw Error('Something went wrong with get /deletetask/:idtodo/:idtask')

        res.status(200).send({msg: 'ok'});

    } catch (error) {
        res.status(400).send({msg: error.message});
        console.log(error.message);
    }
});

router.post('/addprojects', auth, async (req, res) => {
    try {
        const name = req.body.name;
        const creator = req.user.id;


        const addprojects = await User.findOneAndUpdate(
            {_id: creator}, {$push: {"projects": {name: name}}}, {new: true});

        if (!addprojects) throw Error('Something went wrong with post /addprojects/:projectid')

        res.status(200).send(addprojects);

    } catch (error) {
        res.status(400).send({msg: error.message});
        console.log(error.message);
    }
});

router.get('/addtodotoproject/:idtodo/:idproject', auth, async (req, res) => {

    try {
        const idtodo = req.params['idtodo'];
        const idproject = req.params['idproject'];


        const addtodotoproject = await Todo.findOneAndUpdate(
            {_id: idtodo}, {$set: {"project": idproject}}, {new: true});

        if (!addtodotoproject) throw Error('Something went wrong with get /addtodotoproject/:todoid/:projectid');

        res.status(200).send(addtodotoproject);

    } catch (error) {
        res.status(400).send({msg: error.message});
        console.log(error.message);
    }
});

router.get('/deletetodofromproject/:idtodo/:idproject', auth, async (req, res) => {

    try {
        const idtodo = req.params['idtodo'];
        const idproject = req.params['idproject'];


        const deletetodofromproject = await Todo.findOneAndUpdate(
            {_id: idtodo}, {$set: {"project": ''}}, {new: true});

        if (!deletetodofromproject) throw Error('Something went wrong with get /deletetodofromproject/:idtodo/:idproject');

        res.status(200).send(deletetodofromproject);

    } catch (error) {
        res.status(400).send({msg: error.message});
        console.log(error.message);
    }
});


module.exports = router;
