var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var auth = require('../middlewares/auth');
var User = require('../schemas/User');

const jwtSecret = "sl_myJwtSecret";


/* GET auth listing. */
router.post('/login', async (req, res) => {
    const {email, password} = req.body;

    if (!email || !password) {
        return res.status(400).json({msg: "Please enter all fields"});
    }

    try {
        const user = await User.findOne({email});
        if (!user) throw Error('User does not exist');

        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch) throw Error('Invalid Creditianals');

        const token = jwt.sign({id: user._id}, jwtSecret, {expiresIn: '720h'});
        if (!token) throw Error('Could not sign the token');

        res.status(200).json({
            token,
            user: {
                id: user._id,
                name: user.name,
                email: user.email,
                projects: user.projects
            }
        });
    } catch (error) {
        res.status(400).json({msg: error.message});
    }
});

/* post  register listing. */
router.post('/register', async (req, res) => {
    const {name, email, password} = req.body;

    if (!name || !email || !password) {
        return res.status(400).json({msg: 'Please enter all fields'});
    }

    try {
        const user = await User.findOne({email});
        if (user) throw Error('User with this email already exist');

        const salt = await bcrypt.genSalt(10);
        if (!salt) throw Error('Something went wrong with bcrypt');

        const hash = await bcrypt.hash(password, salt);
        if (!hash) throw Error('Something went wrong hashing the password');

        const newUser = new User({
            name,
            email,
            password: hash
        });

        const savedUser = await newUser.save();
        if (!savedUser) throw Error('Something went wrong saving the user');

        const token = jwt.sign({id: savedUser._id}, jwtSecret, {
            expiresIn: '720h'
        });

        res.status(200).json({
            token,
            user: {
                id: savedUser._id,
                name: savedUser.name,
                email: savedUser.email,
                projects: savedUser.projects
            }
        });
    } catch (error) {
        res.status(400).json({msg: error.message});
    }
});

/* GET user info listening. */
router.get('/user', auth, async (req, res) => {
    try {
        const user = await User.findById(req.user.id).select('-password');
        if (!user) throw Error('User does not exist');
        res.status(200).json( {
            id: user._id,
            name: user.name,
            email: user.email,
            register_date: user.register_date,
            projects: user.projects
        });

    } catch (error) {
        res.status(400).json({ msg: error.message });
    }

});

module.exports = router;
