var express = require('express');
var router = express.Router();

var Chat = require('../schemas/Chat');

var auth = require('../middlewares/auth');

router.get('/', auth, async (req, res) => {
    try {
        const chats = await Chat.find({});

        if (!chats) console.log('Error in chat get / ');

        res.status(200).send(chats);

    } catch (e) {

        res.status(400).send(e);

    }
});

router.post('/createchat', auth, async (req, res) => {
    try {
        const chatname = req.body.name;

        const newChat = new Chat({
            name: chatname,
        });

        if (!newChat) console.log('Error in chat post / ');

        const savedChat = await newChat.save();

        if (!savedChat) console.log('Error in savedChat post / ');

        res.status(200).send(savedChat);

    } catch (e) {

        res.status(400).send(e);

    }
});


module.exports = router;