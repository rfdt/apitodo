const {Types} = require('mongoose');

var express = require('express');
var router = express.Router();
var auth = require('../middlewares/auth');
var Tictac = require('../schemas/Tictac');

router.get('/', async (req, res) => {
    try {
        const tictacs = await Tictac.find({});

        if (!tictacs) console.log('Error in chat get / ');

        res.status(200).send(tictacs);

    } catch (e) {

        res.status(400).send(e);

    }
});

router.post('/createroom', async (req, res) => {
    try {
        const roomName = req.body.name;

        const newRoom = new Tictac({
            name: roomName,
        });

        if (!newRoom) console.log('Error in room tictac post / ');

        const savedRoom = await newRoom.save();

        if (!savedRoom) console.log('Error in savedRoom post / ');

        res.status(200).send(savedRoom);

    } catch (e) {
        res.status(400).send(e);
    }
});


module.exports = router;