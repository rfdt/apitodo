var jwt = require('jsonwebtoken');

const jwtSecret = "sl_myJwtSecret";

const auth = (req, res, next) => {
    const token = req.header('todo-auth-token');

    if (!token) {
        return res.status(401).json({msg: "No token, bad request"});
    }

    try {

        const decoded = jwt.verify(token, jwtSecret);

        req.user = decoded;

        next();
    } catch (error) {
        res.status(400).json({msg: "Token is invalid"});
    }
};

module.exports = auth;
