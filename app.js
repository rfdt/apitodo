var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
var mongoose = require('mongoose');


var authRouter = require('./routes/auth');
var todoRouter = require('./routes/todo');
var chatRouter = require('./routes/chat');
var tictacRouter = require('./routes/tictac');

const db = "mongodb://localhost:27017/todoapp";


var app = express();

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

mongoose.connect(db,
    {
        useNewUrlParser:true,
        useCreateIndex: true,
        useUnifiedTopology:true,
        useFindAndModify: false,
    })
    .then(()=> console.log('MongoDb connected'))
    .catch((err)=> console.log('MongoDb ERROR '+err));

app.use('/auth', authRouter);
app.use('/todo', todoRouter);
app.use('/chat', chatRouter);
app.use('/tictac', tictacRouter);

module.exports = app;
