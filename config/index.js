var dotenv =  require('dotenv');

dotenv.config();

export default {
    db: process.env.db,
    jwtSecret: process.env.jwtSecret
};