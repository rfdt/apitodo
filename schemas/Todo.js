const {Schema, model} = require('mongoose');
const { uuid } = require('uuidv4');

const TodoSchema = new Schema({
    data: {
        type: String,
        required: true
    },
    completed: {
        type: Boolean,
        default: false,
    },
    date: {
        type: Date,
        default: Date.now
    },
    creator:{
        type: String,
        required: true
    },
    tasks:{
      type: [{
        data: {
            type: String,
            required: true
        },
        completed: {
            type: Boolean,
            default: false,
        },
        date: {
            type: Date,
            default: Date.now
        }
      }],
      default:[]
    },
    project: {
      type: String,
      default: ''
    }
});

const Todo = model('Todo', TodoSchema);

module.exports = Todo;
