const {Schema, model} = require('mongoose');

const ChatSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    users: {
        type: [{
            name: {
                type: String,
                required: true
            },
            userId:{
                type: String,
                required: true
            },
            socketId: {
                type: String,
                required: true
            },
        }],
        default: []
    },
    messages: {
        type: [{
            text: {
                type: String,
                required: true
            },
            creatorId: {
                type: String,
                required: true
            },
            creatorName: {
                type: String,
                required: true
            },
        }],
        default:[]
    }
});

const Chat = model('Chat', ChatSchema);

module.exports = Chat;
