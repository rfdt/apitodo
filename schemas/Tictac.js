const {Schema, model} = require('mongoose');

const TictacSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    status: {
        type: String,
        default: 'Not Started'
    },
    players: {
        playerX:{
            socket: {
                type:String,
                default: '',
            },
            _id: {
                type:String,
                default: '',
            },
            playerName: {
                type:String,
                default: '',
            },
        },
        playerO:{
            socket: {
                type:String,
                default: '',
            },
            _id: {
                type:String,
                default: '',
            },
            playerName: {
                type:String,
                default: '',
            },
        }
    },
    currentTurn: {
        type: String,
        default: 'X'
    },
    cells: {
        type: [{
            value: String
        }],
        default:[
            {
                value: ''
            },
            {
                value: ''
            },
            {
                value: ''
            },
            {
                value: ''
            },
            {
                value: ''
            },
            {
                value: ''
            },
            {
                value: ''
            },
            {
                value: ''
            },
            {
                value: ''
            },
        ]
    },
    winner: {
        type: String,
        default: ''
    }
});

const Tictac = model('Tictac', TictacSchema);

module.exports = Tictac;
